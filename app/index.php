<?php

mb_internal_encoding("UTF-8");

define('APP_ENV', 'production');
defined('__ROOT__') or define('__ROOT__', dirname(dirname(__FILE__)));

require implode(DIRECTORY_SEPARATOR, array(__ROOT__, 'vendor', 'autoload.php'));

//Code here