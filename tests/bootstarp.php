<?php

mb_internal_encoding("UTF-8");

defined('__ROOT__') or define('__ROOT__', dirname(__FILE__));

require implode(DIRECTORY_SEPARATOR, array(__ROOT__, 'vendor', 'autoload.php'));

if (file_exists(__ROOT__ .'/lib/core/tests/bootstrap.php')) {
    require_once __ROOT__ .'/lib/core/tests/bootstrap.php';
}